import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class SearchSelenium {


    @Test
    void seleniumLink() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Vugar\\Downloads\\chromedriver_win32_new\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        try {
            webDriver.get("https://google.com");
            WebElement searchField = webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf > " +
                    "form > div:nth-child(1) > div.A8SBwf > div.RNNXgb > div > div.a4bIc > input"));
            searchField.sendKeys("Selenium");
            WebElement searchButton = webDriver.findElement(By.cssSelector("body > div.L3eUgb > div.o3j99.ikrT4e.om7nvf >" +
                    " form > div:nth-child(1) > div.A8SBwf > div.FPdoLc.tfB0Bf > center > input.gNO89b"));
            searchButton.click();
            WebElement seleniumLink = webDriver.findElement(By.cssSelector("#rso > div > div:nth-child(1) > div > " +
                    "div.yuRUbf > a > div > cite"));
            seleniumLink.click();
            boolean selPage = webDriver.findElement(By.cssSelector("body > section.getting-started.dark-background > div > " +
                    "div:nth-child(1) > div.download-button-container > a > div")).isDisplayed();
            assertTrue(selPage);
        } finally {
            webDriver.close();
        }
    }
}
