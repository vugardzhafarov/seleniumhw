import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Etsyboots {
    private static final int EXPECTED_COUNT_DISCOUNT = 5;
    private static final int EXPECTED_COUNT = 64;

    @Test
    void Boots() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Vugar\\Downloads\\chromedriver_win32_new\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);
        try {
            webDriver.get("https://www.etsy.com/");
            webDriver.manage().window().maximize();
            Actions actions = new Actions(webDriver);
            Thread.sleep(3000);
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"catnav-primary-link-10923\"]")));
            WebElement clothingShoes = webDriver.findElement(By.xpath("//*[@id=\"catnav-primary-link-10923\"]"));
            actions.moveToElement(clothingShoes).build().perform();
            webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"side-nav-category-link-10936\"]")));
            WebElement men = webDriver.findElement(By.xpath("//*[@id=\"side-nav-category-link-10936\"]"));
            men.click();
            WebElement boots = webDriver.findElement(By.xpath("//*[@id=\"catnav-l4-11109\"]"));
            boots.click();
            List<WebElement> discount = webDriver.findElements(By.className("text-body-smaller " +
                    "promotion-price normal no-wrap "));
            assertEquals(EXPECTED_COUNT_DISCOUNT, discount.size());


        } finally {
            webDriver.close();
        }

    }

    @Test
    void CountOfBoots() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Vugar\\Downloads\\chromedriver_win32_new\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);
        try {
            webDriver.get("https://www.etsy.com/");
            webDriver.manage().window().maximize();
            Actions actions = new Actions(webDriver);
            Thread.sleep(3000);
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"catnav-primary-link-10923\"]")));
            WebElement clothingShoes = webDriver.findElement(By.xpath("//*[@id=\"catnav-primary-link-10923\"]"));
            actions.moveToElement(clothingShoes).build().perform();
            webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"side-nav-category-link-10936\"]")));
            WebElement men = webDriver.findElement(By.xpath("//*[@id=\"side-nav-category-link-10936\"]"));
            men.click();
            WebElement boots = webDriver.findElement(By.xpath("//*[@id=\"catnav-l4-11109\"]"));
            boots.click();
            List<WebElement> discount = webDriver.findElements(By.cssSelector("#content > div >" +
                    " div.content.bg-white.col-md-12.pl-xs-1.pr-xs-0.pr-md-1.pl-lg-0.pr-lg-0.wt-bb-xs-1 > div >" +
                    " div.wt-mt-xs-2.wt-text-black > div.col-group.pl-xs-0.search-listings-group.pr-xs-1 > " +
                    "div:nth-child(2) > div.bg-white.display-block.pb-xs-2.mt-xs-0 > div > div > ul > li"));
            assertEquals(EXPECTED_COUNT, discount.size());


        } finally {
            webDriver.close();
        }
    }
}

