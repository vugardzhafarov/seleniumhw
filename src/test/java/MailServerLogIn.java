import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.ObjectInput;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MailServerLogIn {
    @Test
    void MailLogIn() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Vugar\\Downloads\\chromedriver_win32_new\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);
        try {
            webDriver.manage().window().maximize();
            webDriver.get("https://mail.ru/");
            WebElement username = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                    "div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input"));
            username.sendKeys("ibatech@bk.ru");
            WebElement next = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                    "button.button.svelte-1eyrl7y"));
            next.click();
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[2]/input")));
            WebElement psw = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[2]/input"));
            psw.sendKeys("K5ek4@.NL*s8f2u");
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mailbox\"]/form[1]/button[2]")));
            WebElement login = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/button[2]"));
            login.click();
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"app-canvas\"]/div/div[1]/" +
                    "div[1]/div/div[2]/span/div[1]/div[1]/div/div/div/div[1]/div/div/a/span/span")));
            boolean composeMail = webDriver.findElement(By.cssSelector("#app-canvas > div > div.application-mail > " +
                    "div.application-mail__overlay > div > div.application-mail__layout.application-mail__layout_main > " +
                    "span > div.layout__column.layout__column_left > div.layout__column-wrapper > div > div > div > " +
                    "div:nth-child(1) > div > div > a > span > span")).isDisplayed();
            assertTrue(composeMail);
        } finally {
            webDriver.close();
        }
    }

    @Test
    void InvalidMailLogIn() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Vugar\\Downloads\\chromedriver_win32_new\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);
        try {
            webDriver.manage().window().maximize();
            webDriver.get("https://mail.ru/");
            WebElement username = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                    "div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input"));
            username.sendKeys("ibatech@bk.ru");
            WebElement next = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                    "button.button.svelte-1eyrl7y"));
            next.click();
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[2]/input")));
            WebElement psw = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[2]/input"));
            psw.sendKeys("K5ek4@.NL*s8f2u2");
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mailbox\"]/form[1]/button[2]")));
            WebElement login = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/button[2]"));
            login.click();
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#mailbox > " +
                    "form.body.svelte-1eyrl7y > div.error.svelte-1eyrl7y")));
            boolean warning = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y >" +
                    " div.error.svelte-1eyrl7y")).isDisplayed();
            assertTrue(warning);
        } finally {
            webDriver.close();
        }
    }

    @Test
    void SendMail() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Vugar\\Downloads\\chromedriver_win32_new\\chromedriver.exe");
        WebDriver webDriver = new ChromeDriver();
        WebDriverWait webDriverWait = new WebDriverWait(webDriver, 10);
        try {
            webDriver.manage().window().maximize();
            webDriver.get("https://mail.ru/");
            WebElement username = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                    "div.email-container.svelte-1eyrl7y > div.email-input-container.svelte-1eyrl7y > input"));
            username.sendKeys("ibatech@bk.ru");
            WebElement next = webDriver.findElement(By.cssSelector("#mailbox > form.body.svelte-1eyrl7y > " +
                    "button.button.svelte-1eyrl7y"));
            next.click();
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[2]/input")));
            WebElement psw = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/div[2]/input"));
            psw.sendKeys("K5ek4@.NL*s8f2u");
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mailbox\"]/form[1]/button[2]")));
            WebElement login = webDriver.findElement(By.xpath("//*[@id=\"mailbox\"]/form[1]/button[2]"));
            login.click();
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"app-canvas\"]/div/div[1]/" +
                    "div[1]/div/div[2]/span/div[1]/div[1]/div/div/div/div[1]/div/div/a/span/span")));
            WebElement composeMail = webDriver.findElement(By.cssSelector("#app-canvas > div > div.application-mail > " +
                    "div.application-mail__overlay > div > div.application-mail__layout.application-mail__layout_main > " +
                    "span > div.layout__column.layout__column_left > div.layout__column-wrapper > div > div > div > " +
                    "div:nth-child(1) > div > div > a > span > span"));
            composeMail.click();
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[15]/div[2]/div/div[1]/div[2]/div[3]" +
                            "/div[2]/div/div/div[1]/div/div[2]/div/div/label/div/div/input")));
            WebElement toWhom = webDriver.findElement(By.xpath("/html/body/div[15]/div[2]/div/div[1]/div[2]/div[3]" +
                    "/div[2]/div/div/div[1]/div/div[2]/div/div/label/div/div/input"));
            toWhom.sendKeys("vugardzhafarov@gmail.com");
            Thread.sleep(3000);
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[15]/div[2]/div/div[2]/div[1]/span[1]/span/span")));
            webDriver.findElement(By.className("desktopInput--3cWPE")).sendKeys(("C:\\Users\\Vugar\\Desktop\\22222.pdf"));
            WebElement sendButton = webDriver.findElement(By.xpath("/html/body/div[15]/div[2]/div/div[2]/div[1]/span[1]/span/span"));
            sendButton.click();
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[9]/div/div/div[2]/div[2]/div/div/div[2]/a")));
            boolean mailsent = webDriver.findElement(By.xpath("/html/body/div[9]/div/div/div[2]/div[2]/div/div/div[2]/a")).isDisplayed();
            assertTrue(mailsent);

        } finally {
           webDriver.close();
        }
    }

}
